% MISCELLANEA

\begin{thm}[forma canonica delle involuzioni] $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale con $ \dim{V} = n $ e sia $ f \colon V \to V $ un'applicazione lineare tale che $ f^{2} = \Id $. Allora esiste una base $ \mathscr{B} $ di $ V $ tale che
	\[[f]_{\mathscr{B}}^{\mathscr{B}} =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ \Id_k $} & \\
	& \fbox{$ -\Id_{n - k} $}
	\end{pmatrix}\]
	con $ k \in \N $ univocamente determinato.
\end{thm}
\begin{proof}
	Per ipotesi $ f^2 - \Id $ è l'endomorfismo nullo e quindi il polinomio minimo $ \mu_f(t) $ divide il polinomio $ p(t) = t^2 - 1 = (t - 1)(t + 1) $. Si presentano allora tre casi:
	\begin{itemize}
		\item Se $ \mu_f(t) = t - 1 $, allora per definizione di polinomio minimo $ f - \Id $ è l'endomorfismo nullo ovvero esiste una base in cui $ [f]_{\mathscr{B}}^{\mathscr{B}} = \Id_n $.
		\item Se $ \mu_f(t) = t + 1 $, allora per definizione di polinomio minimo $ f + \Id $ è l'endomorfismo nullo ovvero esiste una base in cui $ [f]_{\mathscr{B}}^{\mathscr{B}} = - \Id_n $.
		\item Se $ \mu_f(t) = (t - 1)(t + 1) $, allora vuol dire che $ \lambda_1 = 1 $ e e $ \lambda_2 = -1 $ sono tutti e soli gli autovalori per $ f $ e siano quindi $ V_1 = \ker{(f - \Id)} $ e $ V_{-1} = \ker{(f + \Id)} $ i rispettivi autospazi. Poiché sono autospazi relativi ad autovalori distinti sono in somma diretta. Inoltre se $ v \in V $ possiamo scrivere \[v = \underbrace{\frac{v + fv}{2}}_{\in V_1} + \underbrace{\frac{v - fv}{2}}_{\in V_{-1}}\] in quanto $ (f - \Id)\left(\frac{v + fv}{2}\right) = \frac{1}{2} (fv + f^2v - \Id v - \Id fv) = \frac{1}{2}(fv + v - v - fv) = 0 $ (dove abbiamo usato il fatto che $ f^2 = \Id $) e analogamente $ (f + \Id)\left(\frac{v - fv}{2}\right) = 0 $. Dunque $ V = V_{1} \oplus V_{-1} $ ovvero $ f $ è diagonalizzabile. Così $ f\lvert_{V_{1}} = \Id $ e $ f\lvert_{V_{-1}} = -\Id $ e quindi esiste una base in cui $ [f]_\mathscr{B}^{\mathscr{B}} $ ha la forma cercate con $ k = \dim{V_1} $.
	\end{itemize}
	D'altra parte il polinomio minimo si spezza in fattori lineari e quindi $ f $ risulta diagonalizzabile. Essendo 1 e -1 le uniche possibili radici del polinomio minimo questi sono gli unici autovalori possibili per $ f $ e quindi esiste una base in cui la matrice di $ f $ ha la forma data dalla tesi.
\end{proof}

\begin{thm}[forma canonica delle proiezioni]
	Sia $ V $ un $ \K $-spazio vettoriale con $ \dim{V} = n $ e sia $ f \colon V \to V $ un'applicazione lineare tale che $ f^{2} = f $ (proiezione). Allora esiste una base $ \mathscr{B} $ di $ V $ tale che
	\[[f]_{\mathscr{B}}^{\mathscr{B}} =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ \Id_k $} & \\
	& \fbox{$ 0_{n - k} $}
	\end{pmatrix}\]
	con $ k \in \N $ univocamente determinato.
\end{thm}
\begin{proof}
	Per ipotesi $ f^2 - f $ è l'endomorfismo nullo e quindi il polinomio minimo $ \mu_f(t) $ divide il polinomio $ p(t) = t^2 - t = t(t - 1) $. Si presentano allora tre casi:
	\begin{itemize}
		\item Se $ \mu_f(t) = t $, allora per definizione di polinomio minimo $ f $ è l'endomorfismo nullo ovvero esiste una base in cui $ [f]_{\mathscr{B}}^{\mathscr{B}} = 0_n $.
		\item Se $ \mu_f(t) = t - 1 $, allora per definizione di polinomio minimo $ f - \Id $ è l'endomorfismo nullo ovvero esiste una base in cui $ [f]_{\mathscr{B}}^{\mathscr{B}} = \Id_n $.
		\item Se $ \mu_f(t) = t(t - 1) $, allora vuol dire $ f(f - \Id) v = O_V $ per ogni $ v \in V $. Allora può succedere che $ (f - \Id)v = O_V $ e in questo caso $ v \in V_1 = \ker{(f - \Id)} $ o che $ (f - \Id)v = w \neq O_V $, ma allora necessariamente $ fw = O_V $ ovvero $ w \in V_0 = \ker{f} $. Così $ \im{(f - \Id)} \subseteq \ker{f} $. Poiché $ V_1 $ e $ V_0 $ sono in somma diretta si ha
		\begin{align*}
			\dim{(V_0 \oplus V_1)} & = \dim{V_0} + \dim{V_1} \\
			& = \dim{\ker{f}} + \dim{V} - \dim{\im{(f - \Id)}} \\
			& \geq \dim{\ker{f}} + \dim{V} - \dim{\ker{f}} \\
			& = \dim{V}
		\end{align*}
		da cui concludiamo che $ V = V_0 \oplus V_1 $, ovvero $ f $ è diagonalizzabile ed esiste quindi una base in cui $ [f]_{\mathscr{B}}^{\mathscr{B}} $ ha la forma richiesta.
	\end{itemize}
    D'altra parte il polinomio minimo si spezza in fattori lineari e quindi $ f $ risulta diagonalizzabile. Essendo 1 e 0 le uniche possibili radici del polinomio minimo questi sono gli unici autovalori possibili per $ f $ e quindi esiste una base in cui la matrice di $ f $ ha la forma data dalla tesi.
\end{proof}

\begin{thm} $\dagger$
	Sia $ V $ uno spazio vettoriale su $ \R $ di dimensione $ \geq 1 $ e sia $ f \colon V \to V $ un'applicazione lineare tale che $ f^{2} = - \Id $. Allora esiste una base $ \mathscr{B} $ di $ V $ tale che
	\[[f]_{\mathscr{B}}^{\mathscr{B}} =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	& \fbox{$ -\Id_m $}\\
	\fbox{$ \Id_m $} &
	\end{pmatrix}\]
	con $ m \in \N $ univocamente determinato.
\end{thm}
\begin{proof}
	Osserviamo che se $ \mathscr{B} = \{v_1, \ldots, v_m, f(v_1), \ldots, f(V_m)\} $ è una base, allora $ [f]_{\mathscr{B}}^{\mathscr{B}} $ è la base cercata. \\
	Dato che $ V \neq \{O_V\} $ esiste $ v_1 \in V \setminus \{O_V\} $. I vettori $ v_1 $ e $ f(v_1) $ sono linearmente indipendenti: infatti se fosse $ f(v_1) = \lambda v_1 $ con $ \lambda \in \R $ allora applicando $ f $ si avrebbe $ - v_1 = \lambda^2 v_1 $ ovvero $ (\lambda^2 + 1) v = O_V $ avremmo un assurdo perché $ v \neq O_V $ e se $ \lambda \in \R $ allora $ \lambda^2 + 1 \neq 0 $. \\
	Ci sono allora due possibilità: o $ \{v_1, f(v_1)\} $ è base di $ V $ e quindi abbiamo finito, oppure $ \exists v_2 \in V : v_2 \notin Span{\{v_1, f(v_1)\}} $. Anche in questo caso $ f(v_2) \notin Span{\{v_1, v_2 f(v_1)\}} $. Infatti se fosse $ f(v_2) = \lambda_1 v_1 + \lambda_2 v_2 + \mu_1 f(v_1) $ applicando $ f $ si avrebbe $ - v_2 = \lambda_1 f(v_1) + \lambda_2 f(v_2) - \mu_2 v_2 = \lambda_1 f(v_1) + \lambda_2 (\lambda_1 v_1 + \lambda_2 v_2 + \mu_1 f(v_1)) - \mu_2 v_2 = \lambda_2^2 v_2 + w $ con $ w \in Span{\{v_1, f(v_1)\}} $; così $ (\lambda_2^2 + 1)v_2 + w = 0 $ ed essendo $ \{v_1, v_2 f(v_1)\} $ linearmente indipendenti in particolare deve essere $ \lambda_2^2 + 1 = 0 $ che è impossibile. \\
	Così l'insieme $ \{v_1, v_2 f(v_1), f(v_2)\} $ è linearmente indipendente. Se è una base di $ V $ la tesi è provata, altrimenti si itera il procedimento. L'algoritmo termina sicuramente perché $ V $ ha dimensione finita. \\
	Osserviamo che necessariamente $ V $ ha dimensione pari. Infatti d'altra parte da $ f^2 = -\Id $ deduciamo che se $ \lambda $ è autovalore per $ f $ allora $ \lambda^2 = -1 $ e quindi $ f $ non ha autovalori reali, mentre è ben noto che se $ V $ ha dimensione dispari allora $ f $ ha almeno un autovalore reale (il polinomio minimo ha grado dispari).
\end{proof}

\begin{thm}[forma canonica delle matrici antisimmetriche reali] $\dagger$
	Sia $ A \in \mathrm{Mat}_{n \times n}(\R) $ antisimmetrica. Allora esiste una matrice ortogonale $ M \in O(n) $ tale che
	\[M^{-1} A M = M^{t} A M =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ H_{a_1} $} & & &\\
	& \ \ddots \ & & \\
	& & \fbox{$ H_{a_k} $} & \\
	& & & \fbox{$ 0 $}
	\end{pmatrix}
	\qquad
	\text{ con }
	H_{a_i} =
	\setlength{\arraycolsep}{3pt}
	\begin{pmatrix}
	0 & a_{i} \\
	-a_{i} & 0
	\end{pmatrix}\]
\end{thm}