% PRODOTTI SCALARI

\begin{definition}[prodotto scalare]
	Sia $ V $ un $ \K $-spazio vettoriale. Un prodotto scalare su $ V $ è una funzione \[\sp{\;}{\,} \colon V \times V \to \K\] bilineare e simmetrica:
	\begin{enumerate}[label=(\roman*)]
		\item $ \forall v,w \in V $ vale $ \sp{v}{w} = \sp{w}{v} $
		\item $ \forall v, w, u \in V $ vale $ \sp{v}{w + u} = \sp{v}{w} + \sp{v}{u} $
		\item $ \forall v, w \in V, \forall \lambda \in \K $ vale $ \sp{\lambda v}{w} = \sp{v}{\lambda w} = \lambda \sp{v}{w} $
	\end{enumerate}	
	Con la coppia $ (V, \phi) $ si indica lo spazio vettoriale $ V $ dotato del prodotto scalare $ \phi \colon V \times V \to \K $. 
\end{definition}

\begin{propriety} Alcuni prodotti scalari godono delle seguenti proprietà
	\begin{enumerate}
		\item Un vettore $ v \in V $ tale che $ \sp{v}{v} = 0 $ si dice \emph{isotropo}.
		\item Un prodotto scalare tale che preso un $ v \in V $ \[\forall w \in V \quad \sp{v}{w} = O_V \Rightarrow v = O_V\] si dice \emph{non degenere} mentre per ogni prodotto scalare si ha $ \forall v \in V, \ \sp{v}{O_{V}} = 0 $.
		\item Un prodotto scalare su $ V $ spazio vettoriale sul campo $ \R $ tale che \[ \forall v \in V \quad \sp{v}{v} \geq 0 \quad \mathrm{e} \quad \sp{v}{v} = 0 \iff v = O_V \] si dice \emph{definito positivo}.
	\end{enumerate}
\end{propriety}

\begin{thm}
	Un prodotto scalare è non degenere se e solo se la matrice $ E $ che lo rappresenta ha rango massimo. Ovvero, se $ \{e_1, \ldots, e_n \} $ è base di $ V $
	\[ \rg{E} = \rg{ 
		\begin{pmatrix}
		\sp{e_1}{e_1} & \cdots  & \sp{e_1}{e_n} \\
		\vdots           & \ddots & \vdots \\
		\sp{e_n}{e_1} & \cdots  & \sp{e_n}{e_n} \\
		\end{pmatrix}}
	= n	\]
\end{thm}

\begin{definition}[ortogonalità]
	Sia $ \sp{\,}{} $ un prodotto scalare su $ V $ un $ \K $-spazio vettoriale. Due vettori $ v, w \in V $ si dicono ortogonali se $ \sp{v}{w} = 0 $
\end{definition}

\begin{definition}[norma, distanza, ortogonalità]
	Sia $ \sp{\,}{} $ un prodotto scalare su $ V $ spazio vettoriale su $ \R $ definito positivo. Allora
	\begin{enumerate}
		\item la norma di $ v \in V $ è $ \norm{v} = \sqrt{\sp{v}{v}} $
		\item la distanza tra $ v, w \in V $ è data da $ \norm{v - w} $.
	\end{enumerate}
\end{definition}

\begin{thm}[di Pitagora]
	Sia $ V $ uno spazio vettoriale sul campo $ \R $ con prodotto scalare definito positivo. Dati $ v, w \in V \colon \sp{v}{w} = 0 $	allora \[\norm{v + w}^2 = \norm{v}^2 + \norm{w}^2\]
\end{thm}

\begin{thm}[del parallelogramma]
	Sia $ V $ uno spazio vettoriale sul campo $ \R $ con prodotto scalare definito positivo. Allora $ \forall v, w \in V $ vale \[\norm{v + w}^2 + \norm{v - w}^2 = 2\norm{v}^2 + 2\norm{w}^2\]
\end{thm}

\begin{definition}[componente]
	Dati $ v, w \in V $ chiamiamo coefficiente di Fourier o componente di $ v $ lungo $ w $ lo scalare \[c = \frac{\sp{v}{w}}{\sp{w}{w}}\] In particolare vale $ \sp{v - cw}{w} = 0 $
\end{definition}

\begin{thm}[disuguaglianza di Schwarz]
	Sia $ V $ uno spazio vettoriale sul campo $ \R $ con prodotto scalare definito positivo. Allora $ \forall v, w \in V $ vale \[|\sp{v}{w}| \leq \norm{v} \cdot \norm{w}\]
\end{thm}

\begin{thm}[disuguaglianza triangolare]
	Sia $ V $ uno spazio vettoriale sul campo $ \R $ con prodotto scalare definito positivo. Allora $ \forall v, w \in V $ vale \[\norm{v + w} \leq \norm{v} + \norm{w}\]
\end{thm}

\begin{thm}
	Siano $ v_1, \ldots , v_n \in V $ a due a due perpendicolari ($ \forall i \neq j \; \sp{v_i}{v_j} = 0 $). Allora $ \forall v \in V $ il vettore \[v - \frac{\sp{v}{v_1}}{\sp{v_1}{v_1}}v_1 - \ldots - \frac{\sp{v}{v_n}}{\sp{v_n}{v_n}}v_n\] è ortogonale a ciascuno dei $ v_i $. Risulta inoltre che il vettore $ c_1 v_1 + \ldots + c_n v_n $ (dove $ c_i = \frac{\sp{v}{v_i}}{\sp{v_i}{v_i}} $) è la migliore approssimazione di $ v $ come combinazione lineare dei $ v_i $.
\end{thm}

\begin{thm}
	Siano $ v_1, \ldots , v_n \in V $ a due a due perpendicolari ($ \forall i \neq j \; \sp{v_i}{v_j} = 0 $), sia $ v \in V $ e $ c_i = \frac{\sp{v}{v_i}}{\sp{v_i}{v_i}} $ i coefficienti di Fourier. Allora per ogni $ \lambda_i \in \R $ vale \[\norm{v - \sum_{i = 1}^{n} c_i v_i} \leq \norm{v - \sum_{i = 1}^{n} \lambda_i v_i}.\]
\end{thm}

\begin{thm}[disuguaglianza di Bessel]
	Siano $ e_1, \ldots , e_n \in V $ a due a due perpendicolari e unitari. Dato un $ v \in V $, sia $ c_i = \frac{\sp{v}{e_i}}{\sp{e_i}{e_i}} $. Allora vale \[\sum_{i = 1}^{n}c_i^2 \leq \norm{v}^2\]
\end{thm}

\begin{thm}[ortogonalizzazione di Gram-Schmidt]
	Sia $ V $ uno spazio vettoriale con prodotto scalare non degenere. Siano $ v_1, \ldots , v_n \in V $ vettori linearmente indipendenti. Possiamo allora trovare dei vettori $ u_1,\ldots, u_r $, con $ r \leq n $ ortogonali tra loro e tali che $ \forall i \leq r, \, Span\{v_1, \ldots, v_i\} = Span \{u_1, \ldots, u_i\} $. In particolare basterà procedere in modo induttivo e prendere
	\[\begin{cases}
	u_1 = v_1 \\
	u_{i} = v_i - \frac{\sp{v_i}{u_1}}{\sp{u_1}{u_1}}u_1 - \ldots - \frac{\sp{v_i}{u_{i-1}}}{\sp{u_{i-1}}{u_{i-1}}}u_{i-1}
	\end{cases}\]
\end{thm}
\begin{proof}
	Costruiamo a partire da $ \{v_1, \ldots, v_n\} $ l'insieme $ \{u_1, \ldots, u_n\} $ dato dalla formula ricorsiva definita sopra. Per costruzione $ u_1, \ldots, u_n $ sono ortogonali a due a due. Mostriamo per induzione che $ \forall i \leq n, \, Span\{v_1, \ldots, v_i\} = Span \{u_1, \ldots, u_i\} $. \\
	Per $ i = 1 $ si ha $ Span{\{v_1\}} = Span{\{u_1\}} $ poiché $ v_1 = u_1 $. Per $ i = 2 $, posto $ c = \frac{\sp{v_2}{u_1}}{\sp{u_1}{u_1}} $ si ha $ Span{\{u_1, u_2\}} = Span{\{v_1, v_1 - c u_1\}} = Span{\{v_1, v_1 - c v_1\}} = Span{\{v_1, v_2\}} $. \\
	Assumiamo ora che si abbia $ Span{\{v_1, \ldots, v_i\}} = Span{\{u_1, \ldots, u_i\}} $ allora se $ c_1, \ldots, c_i $ sono i coefficienti di Fourier
	\begin{align*}
		Span{\{u_1, \ldots, u_i, u_{i + 1}\}} & = Span{\{u_1, \ldots, u_i, v_{i + 1} - c_1 u_1 - \ldots - c_i u_i\}} \\
		& = Span{\{u_1, \ldots, u_i, v_{i + 1}\}} \\
		& = Span{\{u_1, \ldots, u_i\}} + Span{\{v_{i + 1}\}} \\
		& = Span{\{v_1, \ldots, v_i\}} + Span{\{v_{i + 1}\}} \\
		& = Span{\{v_1, \ldots, v_i, v_{i + 1}\}}
	\end{align*}
	da cui segue la tesi.
\end{proof}

\begin{corollary}[esistenza della base ortonormale per prodotto scalare definito positivo]
	Dato $ V $ spazio vettoriale con prodotto scalare definito positivo esiste una base ortonormale di $ V $, ossia una base $ \{u_1,\ldots, u_r\} $ tale che $ \forall i \neq j \; \sp{u_i}{u_j} = 0 $ e che $ \forall i \; \norm{u_i} = 1 $.
\end{corollary}

\begin{definition}[ortogonale e radicale]
	Sia $ V $ uno spazio vettoriale dotato di prodotto scalare $ \phi $ e $ W \subseteq V $. Definiamo ortogonale di $ W $ l'insieme $ W^{\perp} = \{v \in V : \forall w \in W, \varsp{v}{w} = 0\} $.\\
	Definiamo radicale di $ V $ l'insieme $ \operatorname{Rad}{(\phi)} = \{v \in V : \forall w \in V, \varsp{v}{w} = 0\} $. Per definizione si ha che $ \operatorname{Rad}{(\phi)} = V^{\perp} $
\end{definition}

\begin{propriety}[dell'ortogonale] $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale dotato di prodotto scalare $ \phi $ e siano $ U, W $ due sottospazi vettoriali di $ V $. Allora:
	\begin{enumerate}[label = (\roman*)]
		\item $ W \subseteq (W^{\perp})^{\perp} $ e se $ \phi $ è non degenere $ W = (W^{\perp})^{\perp} $;
		\item $ W^{\perp} \cap U^{\perp} = (W + U)^{\perp} $;
		\item $ (W \cap U)^{\perp} \supseteq W^{\perp} + U^{\perp} $ e se $ \phi $ è non degenere $ (W \cap U)^{\perp} = W^{\perp} + U^{\perp} $.
		\item $ W \cap W^{\perp} = \operatorname{Rad}{(\phi \lvert_{W})} $
	\end{enumerate}
\end{propriety}

\begin{thm} $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale dotato di prodotto scalare $ \phi $. Sia $ W $ un sottospazio di $ V $ tale che la restrizione del prodotto scalare $ \phi\lvert_{W} $ sia non degenere. Allora \[V = W \oplus W^{\perp}.\] \texttt{(Fatto a lezione)} In particolare l'enunciato vale se il prodotto scalare definito positivo. 
\end{thm}
\begin{proof}
	(definito positivo) Se $ v \in W \cap W^{\perp} $ allora $ \sp{v}{v} = 0 $ che implica $ v = O_{V} $ in quanto il prodotto scalare è definito positivo. Dunque $ W \cap W^{\perp} = \{O_{V}\} $ ovvero sono in somma diretta. Sia $ \{e_1, \ldots, e_r\} $ una base di $ W $. La possiamo completare ad una base $ \{e_1, \ldots, e_r, e_{r + 1}, \ldots, e_n\} $ di $ V $ e con Gram-Schmidt trovare una base $ \{u_1, \ldots, u_r, u_{r + 1}, \ldots, u_n\} $ di $ V $ che per costruzione è tale che $ W = Span{\{e_1, \dots, e_r\}} = Span{\{u_1, \ldots, u_r\}} $. Inoltre sempre per costruzione risulta $ W^{\perp} = Span{\{u_{r+1}, \ldots, u_n\}} $. Così ogni $ v \in V $ si scrive come $ v = a_1 u_1 + \ldots + a_r u_r + b_{r + 1} u_{r + 1} + \ldots + b_n u_n = w + w^{\perp} $ con $ w \in W $ e $ w^{\perp} \in W^{\perp} $. Dunque $ V = W + W^{\perp} = W \oplus W^{\perp} $.
\end{proof}

\begin{definition}[prodotto hermitiano]
	Sia $ V $ uno spazio vettoriale sul campo $ \C $. Un prodotto hermitiano su $ V $ è una funzione \[\sp{\;}{\,} \colon V \times V \to \C \] che soddisfa le seguenti proprietà
	\begin{enumerate}[label=(\roman*)]
		\item $ \forall v,w \in V $ vale $ \sp{v}{w} = \overline{\sp{w}{v}} $ (coniugato)
		\item $ \forall v, w, u \in V $ vale $ \sp{v}{w + u} = \sp{v}{w} + \sp{v}{u} $ e $ \sp{v + w}{u} = \sp{v}{u} + \sp{w}{u} $
		\item $ \forall v, w \in V, \forall \lambda \in \C $ vale $ \sp{\lambda v}{w} = \lambda \sp{v}{w} $ e $ \sp{v}{\lambda w} = \overline{\lambda} \sp{v}{w} $
	\end{enumerate}	
\end{definition}

\begin{definition}[prodotto hermitiano standard]
	Dati due vettori colonna $ v, w \in \C^n $ definiamo il prodotto hermitiano standard come 
	\[v \cdot w = 
	\begin{pmatrix}
	\alpha_1 \\
	\vdots \\
	\alpha_n \\ 
	\end{pmatrix}
	\cdot 
	\begin{pmatrix}
	\beta_1 \\
	\vdots \\
	\beta_n \\ 
	\end{pmatrix}
	= \alpha_1 \overline{\beta_1} + \ldots + \alpha_n \overline{\beta_n}\]
\end{definition}

\begin{thm}[esistenza della base ortogonale per prodotto scalare generico] \label{thm:base_ortogonale}
	Sia $ V $ un $ \K $-spazio vettoriale non banale di dimensione finita dotato di un prodotto scalare $ \sp{\;}{\,} $. Allora $ V $ ha una base ortogonale. 
\end{thm}
\begin{proof}
	Per induzione su $ \dim{V} = n $. \\
	Se $ n = 1 $ l'enunciato è banalmente verificato perché per ogni $ v \in V $, $ \{v\} $ è base ortogonale. \\
	Supponiamo vero l'enunciato per uno spazio di dimensione $ n - 1  $. Ci sono due casi.
	\begin{itemize}
		\item Se $ \forall v \in V, \ \sp{v}{v} = O_{V} $ allora il prodotto scalare è degenere ($ 2 \sp{v}{w} = \sp{v + w}{v + w} - \sp{v}{v} - \sp{w}{w} = 0 $) e quindi ogni base è ortogonale. 
		\item Se $ \exists v_{1} \in V : \sp{v_1}{v_1} \neq 0 $ sia $ V_1 = Span{\{v_1\}} $. Sia $ V_1^{\perp} $ l'ortogonale di $ V_1 $: ogni $ v \in V $ si può scrivere come 
		\[
			v = \underbrace{\left(v - \frac{\sp{v}{v_1}}{\sp{v_1}{v_1}} v_1\right)}_{\in V_1^{\perp}} + \underbrace{\frac{\sp{v}{v_1}}{\sp{v_1}{v_1}} v_1}_{\in V_1}.
		\] 
		Così $ V = V_1 \oplus V_1^{\perp} $: infatti $ V = V_1 + V_1^{\perp} $ e se $ w \in V_1 \cap V_1^{\perp} $ allora $ w = \lambda v_1 $ così 
		\[
			\sp{w}{w} = 0 \Rightarrow \sp{\lambda v_1}{\lambda v_1} = \lambda^2 \sp{v_1}{v_1} = 0 \Rightarrow \lambda = 0
		\] 
		ovvero $ w = O_{V} \Rightarrow V_1 \cap V_1^{\perp} = \{O_{V}\} $. Osserviamo che $ V_1^{\perp} \subset V $ in quanto $ v_1 \notin V_1^{\perp} $ e quindi $ \dim{V_1^{\perp}} < \dim{V} $ ($ \dim{V_1^{\perp}} = n - 1 $). Per ipotesi induttiva $ V_1^{\perp} $ ha una base ortogonale (più esattamente per la restrizione di $ \sp{\;}{\,} $ a $ V_1^{\perp} $), diciamo $ \{v_2, \ldots, v_n\} $. Allora $ \{v_1, v_2, \ldots, v_n\} $  è una base ortogonale di $ V $ in quanto $ \sp{v_i}{v_j} = 0 $ per ogni $ i \neq j $ con $ i, j \geq 2 $ perché elementi della base di $ V_1^{\perp} $ e $ \sp{v_1}{v_i} = 0 $ per ogni $ j \geq 2 $ perché $ v_j \in V_1^{\perp} $. \qedhere
	\end{itemize}
\end{proof}

\begin{thm}[Algoritmo di Lagrange] $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale, $ \sp{\;}{\,} $ un prodotto scalare se $ V $ e $ \mathscr{B} = \{v_1, \ldots, v_n\} $ una base qualunque di $ V $. 
	\begin{enumerate}
		\item Se $ v_1 $ non è isotropo, cioè $ \sp{v_1}{v_1} \neq 0 $, poniamo
		\begin{align*}
		v'_1 & = v_1 \\
		v'_2 & = v_2 - \frac{\sp{v_2}{v'_1}}{\sp{v_1}{v_1}} v'_1 \\
		& \vdotswithin{=} \\
		v'_n & = v_n - \frac{\sp{v_n}{v'_1}}{\sp{v_n}{v_1}} v'_1
		\end{align*}
		Così $ \sp{v'_j}{v'_1} = 0 $ per ogni $ j \in {2, \ldots, n} $ e $ \mathscr{B}' $ è una base di $ V $.
		\item Se $ v_1 $ è isotropo, cioè $ \sp{v_1}{v_1} \neq 0 $ allora 
		\begin{enumerate}
			\item Se $ \exists j \in {2, \ldots, n} $ tale che $ \sp{v_j}{v_j} \neq 0 $ permuto la base $ \mathscr{B} $ in modo che $ v_j $ sia il primo vettore e procedo come in 1.
			\item Se $ \forall j \in {1, \ldots, n}, \ \sp{v_j}{v_j} = 0 $ allora ci sono due casi
			\begin{itemize}
				\item $  \sp{\;}{\,} $ è il prodotto scalare nullo e quindi ogni base è ortogonale
				\item $ \exists i \neq j :  \sp{v_i}{v_j} \neq 0 $ in tale caso $ \sp{v_i + v_j}{v_i + v_j} = 2 \sp{v_j}{v_j} \neq 0 $. Scelgo allora una base di $ V $ in cui $ v_i + v_j $ è il primo vettore e procedo come in 1.
			\end{itemize}
		\end{enumerate}
	\end{enumerate}
	Dopo aver ortogonalizzato i vettori rispetto al primo, itero il procedimento su $ \{v'_2, \ldots, v'_n\} $ e così via. Alla fine ottengo una base ortogonale rispetto a $ \sp{\;}{\,} $. Dunque vale l'enunciato del Teorema \ref{thm:base_ortogonale}.
\end{thm}

\begin{prop}[base ortonormale]
	Sia $ \{w_1, \ldots, w_n\} $ una base ortogonale di $ V $ con un prodotto scalare. Posto
	\[v_i = 
	\begin{cases*}
	\frac{w_i}{\sqrt{\sp{w_i}{w_i}}} & se $ \sp{w_i}{w_i} > 0 $ \\
	w_i & se $ \sp{w_i}{w_i} = 0 $ \\
	\frac{w_i}{\sqrt{-\sp{w_i}{w_i}}} & se $ \sp{w_i}{w_i} < 0 $ \\
	\end{cases*}\]
	l'insieme $ \{v_1, \ldots, v_n\} $ è una base ortonormale di $ V $. 
\end{prop}

\begin{thm}[corrispondenza matrice - prodotto scalare]
	Sia $ V $ un $ \K $-spazio vettoriale e sia $ \mathscr{B} = \{v_1, \ldots, v_n\} $ base di $ V $. Dato un prodotto scale $ \sp{\;}{\,} \colon V \times V \to \K $ la matrice del prodotto scalare è \[M_{\mathscr{B}} = (\sp{v_i}{v_j})_{\substack{i = 1, \ldots, n \\ j = 1, \ldots, n}}.\] Viceversa data $ M $ matrice simmetrica del prodotto scalare e $ u, w $ vettori di vettori colonna $ [u]_{\mathscr{B}} $ e $ [w]_{\mathscr{B}} $ si ha \[\sp{v}{w} = [u]_{\mathscr{B}}^t \, M \, [w]_{\mathscr{B}}.\]
\end{thm}

\begin{prop}[cambio di base per un prodotto scalare]
	Sia $ V $ un $ \K $-spazio vettoriale, $ \phi $ un prodotto scalare su $ V $. Siamo $ \mathscr{B} = \{v_1, \ldots, v_n\} $ e $ \mathscr{B}' = \{w_1, \ldots, w_n\} $ basi di $ V $. Allora se $ B $ è la matrice di cambio di base da $ \mathscr{B}' $ a $ \mathscr{B} $ e $ M(\phi) $ è la matrice del prodotto scalare nella rispettiva base vale \[M_{\mathscr{B}'}(\phi) = B^{t} M_{\mathscr{B}}(\phi) B.\]
\end{prop}

\begin{prop}[radicale] \label{thm:radicale}
	Sia $ V $ un $ \K $-spazio vettoriale e $ \phi $ un prodotto scalare su $ V $. Vale che $ \operatorname{Rad}{(\phi)} = \{v \in V : \forall w \in V, \phi(v, w) = 0\} = \{v \in V : M_{\mathscr{B}}(\phi) \cdot [v]_{\mathscr{B}} = 0\} $.\\
	(Moralmente $ \operatorname{Rad}{(\phi)} = \ker{M_{\mathscr{B}}(\phi)} $). 
\end{prop}

\begin{definition}[spazio duale, funzionali]
	Sia $ V $ un $ \K $-spazio vettoriale. Si definisce spazio duale di $ V $ l'insieme delle applicazioni lineari da $ V $ in $ \K $ 
	\[
		V^{*} = \mathscr{L}(V, \K) = \mathscr{L}(V) = \{\varphi \colon V \to \K : \varphi \text{ è lineare}\}.
	\] 
	I suoi elementi vengono detti funzionali lineari da $ V $ in $ \K $ e risulta $ \dim{V} = \dim{V^{*}} $ (poiché $ \mathscr{L}(V) $ è isomorfo a $ \mathrm{Mat}_{1 \times n}(\K) $). 
\end{definition}

\begin{definition}[base duale]
	Sia $ V $ un $ \K $-spazio vettoriale. Fissata una base $ \{v_1, \ldots, v_n\} $ di $ V $ esiste una base $ \{\varphi_1, \ldots, \varphi_n\} $ di $ V^{*} $ ad essa associata detta base duale di $ v_1, \ldots, v_n $ definita come 
	\[\varphi_i (v_j) = \delta_{ij} =  
	\begin{cases*}
	1 & se $ i = j $ \\
	0 & se $ i \neq j $ \\
	\end{cases*}\]
\end{definition}

\begin{thm}[di rappresentazione di Riesz] \label{thm:isom_duale}
	Sia $ V $ un $ \K $-spazio vettoriale di dimensione finita con un prodotto scalare non degenere. Allora l'applicazione
	\begin{align*}
	\Phi \colon V & \to V^{*} \\
	v & \mapsto \varphi_v = \sp{v}{\cdot \,}
	\end{align*}
	dove $ \varphi_v $ è la funzionale tale che $ \forall w \in V : \varphi_v(w) = \sp{v}{w} $, è un isomorfismo tra $ V $ e il suo duale $ V^{*} $. In altri termini, dato $ \varphi \in V^{*} $ esiste un unico $ v \in V $ tale che $ \forall w \in V, \varphi{(w)} = \sp{v}{w} $ . In tale caso si dice che $ \varphi $ è rappresentabile.
\end{thm}

\begin{thm}[annullatore]
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $ e sia $ W $ sottospazio di $ V $. Sia inoltre \[\operatorname{Ann}{W} = \{\varphi \in V^{*} : \forall w \in W, \varphi(w) = 0\}\] l'annullatore di $ W $. Allora $ \operatorname{Ann}{W} $ è sottospazio di $ V^{*} $ e vale che $ \dim{(\operatorname{Ann}{W})} = n - \dim{W} $. 
\end{thm}

\begin{corollary}
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $ con prodotto scalare non degenere, sia $ W $ sottospazio di $ V $ e $ W^{\perp} $ il suo ortogonale. Siano inoltre $ V^{*} $ il duale di $ V $ e $ \operatorname{Ann}{W} $ l'annullatore di $ W $. Allora $ \Phi(W^{\perp}) = \operatorname{Ann}{W} $ (in altre parole $ \Phi|_{W^{\perp}} \colon W^{\perp} \to \operatorname{Ann}{W} $ è un isomorfismo) e vale quindi \[\dim{W} + \dim{W^{\perp}} = \dim{V}.\]
\end{corollary}

\begin{thm} $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale dotato di un prodotto scalare $ \phi $. Sia $ W $ sottospazio di $ V $, $ W^{\perp} $ il suo ortogonale e $ \operatorname{Rad}{(\phi)} $ il radicale di $ \phi $. Allora vale \[\dim{W} + \dim{W^{\perp}} = \dim{V} + \dim{(W \cap \operatorname{Rad}{(\phi)})}\]
\end{thm}

\begin{thm} $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale dotato di un prodotto scalare $ \phi $ e sia
	\begin{align*}
	\Phi \colon V & \to V^{*} \\
	v & \mapsto \varphi_v = \varsp{v}{\cdot \,}
	\end{align*}
	l'applicazione lineare del Teorema \ref{thm:isom_duale}. Allora $ \operatorname{Rad}{(\phi)} = \ker{\Phi} $ e $ M_{\mathscr{B^{*}}}^{\mathscr{B}} (\Phi) = M_{\mathscr{B}}(\phi) $ dove $ \mathscr{B}^{*} $ è la base del duale associata alla base $ \mathscr{B} $ di $ V $. 
\end{thm}

\begin{corollary}
	Un prodotto scalare $ \phi $ su $ V $ è non degenere $ \iff $ $ \operatorname{Rad}{(\phi)} = \{O_V\} $ $ \iff $ $ \ker{M_{\mathscr{B}}(\phi)} = O_V $ $ \iff $ $ \rg{M_{\mathscr{B}}(\phi)} = n = \dim{V} $. 
\end{corollary}

\begin{corollary}
	Se $ \mathscr{B} $ è una base ortonormale allora la matrice del prodotto scalare è diagonale. Detti $ \lambda_i = \phi(v_i, v_i) $ si ha 
	\[M_{\mathscr{B}}(\phi) = 
	\begin{pmatrix}
	\lambda_1 & \cdots & 0 \\
	\vdots & \ddots & \vdots \\
	0 & \ldots & \lambda_n \\
	\end{pmatrix}\]
\end{corollary}

\begin{prop}[indice di nullità]
	Se $ \mathscr{B} = \{v_1, \ldots, v_n\} $ è una base ortonormale di $ V $ rispetto a $ \phi $ prodotto scalare su $ V $, allora \[n_0 (\phi) = \operatorname{card}{\{i : \lambda_i = \phi(v_i, v_i) = 0\}} = \dim{\operatorname{Rad}{(\phi)}}.\] Tale valore viene detto indice di nullità del prodotto scalare. 
\end{prop}
\begin{proof}
	Poiché $ \mathscr{B} $ è base ortonormale, il numero di $ i : \lambda_i = 0 $ è la dimensione del nucleo di $ M_{\mathscr{B}}(\phi) $. Dunque per la Proposizione \ref{thm:radicale} si ha $ n_0(\phi) = \dim{\ker{M_{\mathscr{B}}(\phi)}} = \dim{\operatorname{Rad}{(\phi)}} $.  
\end{proof}

\begin{thm}[di Sylvester, indice di positività e di negatività] \label{thm:Sylvester}
	Sia $ V $ uno spazio vettoriale su $ \R $, $ \phi $ un prodotto scalare su $ V $ e $ \mathscr{B} = \{v_1, \ldots, v_n\} $ una base ortonormale di $ V $. Esiste un numero intero $ r = n_{+} (\phi) $ che dipende solo da $ \phi $ e non dalla base $ \mathscr{B} $, detto indice di positività, tale che ci sono esattamente $ r $ indici $ i $ tali che $ \phi(v_i, v_i) = 1 $. Analogamente esiste un $ r' = n_{-} (\phi) $ che dipende solo da $ \phi $ e non dalla base $ \mathscr{B} $, detto indice di negatività, tale che ci sono esattamente $ r' $ indici $ i $ tali che $ \phi(v_i, v_i) = - 1 $.
\end{thm}
\begin{proof}
	Siano $ \{v_1, \ldots, v_n\} $ e $ \{w_1, \ldots, w_n\} $ basi ortonormali di $ V $ ordinate in modo tale che 
	\begin{gather*}
		\phi(v_1, v_1) > 0, \ldots, \phi(v_r, v_r) > 0, \phi(v_{r + 1}, v_{r + 1}) \leq 0, \ldots, \phi(v_n, v_n) \leq 0; \\
		\phi(w_1, w_1) > 0, \ldots, \phi(w_{r'}, w_{r'}) > 0, \phi(w_{r' + 1}, w_{r' + 1}) \leq 0, \ldots, \phi(w_n, w_n) \leq 0.
	\end{gather*}
	Consideriamo l'insieme $ \{v_1, \ldots, v_r, w_{r' + 1}, \ldots, w_n\} $ e supponiamo che esistano $ \lambda_1, \ldots, \lambda_r, \mu_{r' + 1}, \ldots, \mu_n \in \K $ tali che \[\lambda_1 v_1 + \ldots + \lambda_r v_r + \mu_{r' + 1} w_{r' + 1} + \ldots + \mu_n w_n = O_{V}.\] Allora posto $ v = \lambda_1 v_1 + \ldots + \lambda_r v_r = - \mu_{r' + 1} w_{r' + 1} - \ldots - \mu_n w_n $ si ha da un lato \[\phi(v, v) = \phi\left(\sum_{i = 1}^{r} \lambda_i v_i, \sum_{j = 1}^{r} \lambda_j v_j\right) = \sum_{i = 1}^{r} \sum_{j = 1}^{r} \lambda_i \lambda_j \phi(v_i, v_j) = \sum_{i = 1}^{r} \lambda_i^2 \phi(v_i, v_i) \geq 0\] e d'altra parte \[\phi(v, v) = \phi\left( - \sum_{i = r' + 1}^{n} \mu_i w_i,  - \sum_{j = r' + 1}^{n} \mu_j w_j\right) = \sum_{i = r' + 1}^{n} \sum_{j = r' + 1}^{n} \mu_i \mu_j \phi(w_i, w_j) = \sum_{i = r' + 1}^{n} \mu_i^2 \phi(w_i, w_i) \leq 0.\] Dunque $ \phi(v, v) = 0 $, così $ \sum_{i = 1}^{r} \lambda_i^2 \phi(v_i, v_i) = 0 $ ovvero $ \lambda_1 = \ldots = \lambda_r = 0 $. Ne segue che $ \sum_{i = r' + 1}^{n} \mu_i w_i = - v = 0 $ da cui per la lineare indipendenza dei $ w_i $ segue che $ \mu_{r' + 1} = \ldots = \mu_n = 0 $. Dunque $ v_1, \ldots, v_r, w_{r' + 1}, \ldots, w_n $ sono linearmente indipendenti. Così, poiché $ \dim{V} = n $, deve essere $ r + (n - r') \leq n $ ovvero $ r \leq r' $. Ma allora ripetendo lo stesso argomento su $ \{w_1, \ldots, w_{r'}, v_{r + 1}, \ldots, v_n\} $ otteniamo $ r' \leq r $. Dunque $ r = r' $ che è la tesi. \\
	Per dimostrare la tesi sull'indice di negatività possiamo operare in due modi. Ripetiamo la dimostrazione ordinando la base in modo che $ \phi(v_i, v_i) = \phi(w_j, w_j) < 0 $ per $ i = 1, \ldots, r $ e per $ j = 1, \ldots, r' $. Oppure osserviamo che nella base ortonormale la matrice $ M_{\mathscr{B}}(\phi) $ è diagonale e ha sulla diagonale $ \phi(v_i, v_i) $; così poiché deve essere $ n_+ + n_- + n_0 = n $ e $ n_+, n_0, n $ non dipendono dalla base anche $ n_- $ non dipende dalla base. 
\end{proof}

\begin{definition}[segnatura e forma canonica dei prodotti scalari]
	Si definisce segnatura di un prodotto scalare $ \phi $ su $ V $ spazio vettoriale su $ \R $ la terna $ (n_0 (\phi), n_{+} (\phi), n_{-} (\phi)) $. Detta $ n = \dim{V} $ vale \[n_0 (\phi) + n_{+} (\phi) + n_{-} (\phi) = n.\] Valgono le seguenti caratterizzazioni
	\begin{itemize}
		\item $ n_0 (\phi) = \dim{\operatorname{Rad}{(\phi)}} $;
		\item $ n_{+} (\phi) = \max{\{\dim{W} : W \text{ è stottospazio di } V \text{ e } \phi|_{W} > 0\}} $;
		\item $ n_{-} (\phi) = \max{\{\dim{W} :W \text{ è stottospazio di } V \text{ e } \phi|_{W} < 0\}} $.
	\end{itemize}
	Per il Teorema \ref{thm:Sylvester} esiste una base ortonormale $ \mathscr{B} $ rispetto a $ \phi $ in cui la matrice del prodotto scalare è della forma
	\[M_{\mathscr{B}}(\phi) =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ \Id_{n_{+}} $} & & \\
	& \fbox{$ -\Id_{n_{-}} $} & \\
	& & \fbox{$ 0_{n_{0}} $}
	\end{pmatrix} \]
	dove $ \Id_{r} $ è la matrice identità di dimensione $ r \times r $ e $ 0_p $ è la matrice nulla di dimensione $ p \times p $. \\
	\linebreak
	Operativamente: per trovare la segnatura scrivo la matrice del prodotto scalare in una base ortonormale rispetto al prodotto scalare $ \phi $; tale matrice è diagonale e la segnatura si legge sugli elementi della diagonale (ci sono $ n_{+} $ elementi uguali a 1, $ n_{-} $ elementi uguali a -1 e $ n_{0} $ elementi nulli). Per il Teorema Spettrale, posso semplicemente trovare gli autovalori della matrice del prodotto scalare (\textbf{Ci vanno altre ipotesi??})
\end{definition}