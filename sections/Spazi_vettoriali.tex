% SAPZI VETTORIALI

\begin{definition}[campo] \footnote{Riassunto della sezione 1}
	Un campo è una terna $ (\K, +, \cdot) $ dove $ \K $ è un insieme su cui sono definite due operazioni di \emph{somma} $ + \colon \K \times \K \to \K $ e \emph{prodotto} $ \cdot \colon \K \times \K \to \K $ che associano a due elementi dell'insieme un altro elemento dell'insieme, ovvero tali che
	\begin{enumerate}
		\item $ \forall x, y \in \K \Rightarrow x + y \in \K $
		\item $ \forall x, y \in \K \Rightarrow x \cdot y \in \K $
	\end{enumerate}
	e che rispettano le seguenti proprietà
	\begin{enumerate}[label = (\roman*)]
		\item \emph{associativa}: $ \forall x, y, z \in \K, \ x + (y + z) = (x + y) + z \text{ e } x \cdot (y \cdot z) = (x \cdot y) \cdot z $;
		\item \emph{commutativa}: $ \forall x, y \in \K, \ x + y = y + x \text{ e } x \cdot y = y \cdot x $;
		\item \emph{esistenza degli elementi neutri}: $ \exists 0_{\K}, 1_{\K} \in \K: 0_{\K} \neq 1_{\K} $ tali che $ \forall x \in \K, \ x + 0_{\K} = 0_{\K} + x = x $ e $ \forall x \in \K, \ x \cdot 1_{\K} = 1_{\K} \cdot x = x $ (tali elementi sono unici e per semplicità vengono indicati con $ 0 $ e $ 1 $);
		\item \emph{opposto}: $ \forall x \in \K, \exists y \in \K : x + y = 0_{\K} $ che viene indicato con $ - x $;
		\item \emph{inverso}: $ \forall x \in \K : x \neq 0_{\K}, \exists y \in \K : x \cdot y = 1_{\K} $ che viene indicato con $ \frac{1}{x} $ o $ x^{-1} $;
		\item \emph{distributiva del prodotto rispetto alla somma}: $ \forall x, y, z \in \K, \ x \cdot (y + z) = x \cdot y + x \cdot z $.
	\end{enumerate}
\end{definition}

\begin{definition}[spazio vettoriale]
	Uno spazio vettoriale $ V $ su un campo $ \K $ o $ \K $-spazio vettoriale è una quaterna $ (V, \K, +, \cdot) $ dove $ \K $ è un campo e $ V $ è un insieme non vuoto su cui sono definite due operazioni di 
	\begin{enumerate}
		\item \emph{somma} $ + \colon V \times V \to V $ tra elementi di $ V $ tale che $ \forall v, w \in V \Rightarrow + (v, w) = v + w \in V $
		\item \emph{prodotto per scalare} $ \cdot \colon \K \times V \to V $ tale che $ \forall v \in V, \forall \lambda \in \K \Rightarrow \cdot (\lambda, v) = \lambda \cdot v = \lambda v \in V $
	\end{enumerate}
	che devono rispettare le seguenti proprietà:
	\begin{enumerate}[label=(\roman*)]
		\item \emph{associativa della somma}: $ \forall v, w, u \in V, \ v + (w + u) = (v + w) + u $;
		\item \emph{commutativa della somma}: $ \forall v, w \in V, \ v + w = w + v $;
		\item \emph{esistenza dell'elemento neutro della somma}: $ \exists O_{V} \in V : \forall v \in V, \ v + O_{V} = O_{V} + v = v $;
		\item \emph{opposto della somma}: $ \forall v \in V, \exists w \in W : v + w = O_{V} $ che viene indicato con $ w = -v $;
		\item \emph{distributiva del prodotto rispetto alla somma}: $ \forall \lambda \in \K, \forall v, w \in V, \ \lambda (v + w) = \lambda v + \lambda w $;
		\item \emph{distributiva del somma sul campo rispetto al prodotto}: $ \forall \lambda, \mu \in \K, \forall v \in V, \ (\lambda + \mu) v = \lambda v + \mu v $;
		\item \emph{associativa del prodotto}: $ \forall \lambda, \mu \in \K, \forall v \in V, \ (\lambda \mu) v = \lambda (\mu v) $;
		\item \emph{elemento neutro del prodotto}: $ \exists 1_{\K} \in \K : \forall v \in V, \ 1_{\K} \cdot v = v $;
	\end{enumerate}
	Chiameremo \emph{vettori} gli elementi di $ V $. \\
	\textsf{Nota: dove non specificato intenderemo sempre che $ V $ è uno spazio vettoriale su un campo $ \K $.}
\end{definition}

\begin{propriety} Uno spazio vettoriale gode delle seguenti proprietà:
	\begin{enumerate}
		\item Unicità dell'elemento neutro
		\item Unicità dell'opposto
		\item $ 0_{\K} \cdot v = O_V $
		\item $ (-1_{\K}) \cdot v = -v $
	\end{enumerate}
\end{propriety}

\begin{definition}[sottospazio vettoriale]
	Sia $ V $ un $ \K $-spazio vettoriale. Diciamo che $ W \subseteq V $ è un sottospazio vettoriale di $ V $ se valgono le seguenti proprietà
	\begin{enumerate}[label=(\roman*)]
		\item $ \forall v, w \in W \Rightarrow v + w \in W $
		\item $ \forall v \in W, \forall \lambda \in \K \Rightarrow \lambda v \in W $
		\item $ O_V \in W $
	\end{enumerate}
\end{definition}

\begin{thm}[intersezione di sottospazi]
	Sia $ V $ un $ \K $-spazio vettoriale e $ W, U \subseteq V $ sottospazi di $ V $. Allora $ W \cap U $ è sottospazio di $ V $. 
\end{thm}

\begin{thm}[somma di sottospazi]
	Sia $ V $ un $ \K $-spazio vettoriale e $ W, U \subseteq V $ sottospazi di $ V $. Allora \[W + U = \{v \in V : \exists w \in W, u \in U : v = w + u\}\] è un sottospazio vettoriale di $ V $. 
\end{thm}

\begin{definition}[combinazione lineare]
	Sia $ V $ un $ \K $-spazio vettoriale, siano $ v_1, \ldots , v_n \in V $ e $ \lambda_1, \ldots , \lambda_n \in \K $. Si dice combinazione lineare dei $ v_i $ un vettore $ v \in V $ tale che \[v = \sum_{i = 1}^{n} \lambda_i v_i = \lambda_1 v_1 + \ldots + \lambda_k v_n.\]
\end{definition}

\begin{definition}[Span]
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots, v_k \in V $. Si dice $ Span{\{v_1, \ldots , v_n\}} $ l'insieme di tutte le possibili combinazioni lineari dei $ v_i $. Formalmente
	\[Span{\{v_1, \ldots , v_n\}} = \left\{ v \in V: \exists \lambda_1, \ldots , \lambda_n \in \K: v = \sum_{i=1}^{n} \lambda_i v_i \right\}.\]
\end{definition}

\begin{thm}[proprietà dello span]
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots , v_n \in \nolinebreak V $. Allora $ Span\{v_1, \ldots , v_n\} $ è il più piccolo sottospazio vettoriale di $ V $ che contiene tutti i $ v_i $.
\end{thm}

\begin{definition}[dipendenza e indipendenza lineare] 
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots , v_n \in V $. Si dice che $ v_1, \ldots, v_n $ sono linearmente dipendenti se $ \exists \lambda_1, \ldots , \lambda_n \in \K $ non tutti nulli tali che \[\lambda_1 v_1 + \ldots + \lambda_n v_n = O_V.\] Analogamente si dice che $ v_1, \ldots, v_n $ sono linearmente indipendenti se \[\lambda_1 v_1 + \ldots + \lambda_n v_n = O_V \Rightarrow \lambda_1 = \ldots = \lambda_n = 0.\]	
\end{definition}

\begin{definition}[base]
	Sia $ V $ un $ \K $-spazio vettoriale. Un insieme $ \{v_1, \ldots, v_n\} $ si dice base di $ V $ se
	\begin{enumerate}[label=(\roman*)]
		\item $ v_1, \ldots, v_n $ sono linearmente indipendenti;
		\item $ Span\{v_1, \ldots , v_n\} = V $ (generano).
	\end{enumerate}
\end{definition}

\begin{thm}[unicità della combinazione lineare] \label{thm:unicità_base}
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots , v_n \in V $ linearmente indipendenti. Se $ \exists \lambda_1, \ldots, \lambda_n \in \K $ e $ \exists \mu_1, \ldots, \mu_n \in \K $ tali che $ \sum_{i = 1}^{n} \lambda_i v_i = \sum_{i = 1}^{n} \mu_i v_i $, allora $ \lambda_i = \mu_i $ per ogni $ i = 1, \ldots, n $. Dunque la scrittura di un vettore come combinazione lineare di vettori linearmente indipendenti è unica.
\end{thm}

\begin{definition}[sottoinsieme massimale]
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots , v_n \in V $. Diciamo che l'insieme $ \{v_1, \ldots v_r\} $ con $ r \in \N $ e $ r \leq n $ è un sottoinsieme massimale di vettori linearmente indipendenti se $ v_1, \ldots, v_r $ sono linearmente indipendenti e se $ \forall i \in \N : r < i \leq n $, $ v_1, \ldots, v_r, v_i $ sono linearmente dipendenti. 
\end{definition}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale e siano $ v_1, \ldots , v_m \in V : Span{\{v_1, \ldots, v_m\}} = V $ (generano). Sia $ \{v_1, \ldots, v_n\} $ un sottoinsieme di $ \{v_1, \ldots, v_m\} $ linearmente indipendente e massimale. Allora $ \{v_1, \ldots, v_n\} $ è una base di $ V $. 
\end{thm}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale e sia $ \{v_1, \ldots, v_n\} $ una base di $ V $. Se $ w_1, \ldots, w_m $, con $ m > n $, sono vettori di $ V $, allora $ w_1, \ldots, w_m $ sono linearmente dipendenti.
\end{thm}
\begin{proof}
	Se $ \exists j \in \{1, \ldots, m\} $ tale che $ w_j = 0 $ allora $ \forall \lambda \in \K $ si ha $ 0 w_1 + \ldots + \lambda w_j + \ldots + 0 w_m = O_V $ e quindi l'enunciato risulta verificato. Supponiamo quindi che $ w_1, \ldots, w_m $ siano tutti non nulli. Per assurdo $ w_1, \ldots, w_m $ sono linearmente indipendenti. Poiché $ \{v_1, \ldots, v_n\} $ è una base di $ V $ allora $ w_1 = a_1 v_1 + \ldots + a_n v_n $ con $ a_1, \ldots, a_n \in \K $ non tutti nulli. Supponiamo quindi \emph{wlog} $ a_1 \neq 0 $, allora 
	\[
		v_1 = \frac{1}{a_1} w_1 - \frac{a_2}{a_1} v_2 - \ldots - \frac{a_n}{a_1}v_n.
	\]
	Allora $ v_1 \in Span{\{w_1, v_2, \ldots, v_n\}} $ così $ V = Span{\{v_1, \ldots, v_n\}} \subseteq Span{\{w_1, v_2, \ldots, v_n\}} $ essendo il più piccolo sottospazio vettoriale contenente $ v_1, \ldots, v_n $ e quindi $ Span{\{w_1, v_2, \ldots, v_n\}} = V $. \\
	L'idea è di rimpiazzare tutti i $ v_1, \ldots, v_n $ con i $ w_1, \ldots, w_n $ così che $ w_1, \ldots, w_n $ generino $ V $. Procediamo in per induzione: supponiamo esista $ r \in \N : r \leq n $ tale che $ w_1, \ldots, w_r, v_{r + 1}, \ldots, v_n $ generino $ V $. Allora esistono $ b_1, \ldots, b_n \in \K $ tali che $ w_{r + 1} = b_1 w_1 + \ldots b_{r} w_r + b_{r + 1} v_{r + 1} + \ldots + b_n v_n $. Osserviamo che almeno uno tra $ b_{r + 1}, \ldots, b_n $ è non nullo (se fossero tutti nulli otterremmo una relazione di lineare dipendenza tra $ w_1, \ldots, w_m $). Così 
	\[
		v_{r + 1} = - \frac{b_1}{b_{r + 1}} w_1 - \ldots - \frac{b_r}{b_{r + 1}} + \frac{1}{b_{r + 1}} w_{r + 1} - \frac{b_{r + 2}}{b_{r + 1}} v_{r + 2} - \ldots - \frac{b_n}{b_{r + 1}} v_{n}.
	\]
	Dunque $ v_{r + 1} \in Span{\{w_1, \ldots, w_{r + 1}, v_{r + 2}, \ldots, v_n\}} $ così $ V = Span{\{w_1, \ldots, w_r, v_{r + 1}, \ldots, v_n\} }\subseteq Span{\{w_1, \ldots, w_{r + 1}, v_{r + 2}, \ldots, v_n\}} $ quindi $ w_1, \ldots, w_{r + 1}, v_{r + 2}, \ldots, v_n $ generano $ V $. \\
	Per induzione su $ r $ allora $ Span{\{w_1, \ldots, w_n\}} = V $. Ma allora per $ m \geq k > n $ esistono $ d_1, \ldots, d_n \in \K $ non tutti nulli tali che $ w_k = d_1 w_1 + \ldots + d_n w_n $. Così $ w_1, \ldots, w_n, w_k $ non sono linearmente indipendenti da cui l'assurdo. 
\end{proof}


\begin{corollary}
	Sia $ V $ un $ \K $-spazio vettoriale. Supponiamo di avere due basi di $ V $, una con $ n $ elementi e una con $ m $ elementi. Allora $ n = m $.
\end{corollary}

\begin{definition}[dimensione]
	Sia $ V $ un $ \K $-spazio vettoriale avente una base costituita da $ n $ vettori. Allora diremo che $ V $ ha dimensione $ n $ e scriveremo $ \dim{V} = n $. \\ 
	\textsf{Nota: dove non specificato lo spazio considerato ha dimensione finita $ n $.}
\end{definition}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $. Se $ v_1, \ldots, v_n $ generano $ V $ allora $ \{v_1, \ldots, v_n\} $ è una base di $ V $.
\end{thm}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale. Se $ \{v_1, \ldots, v_n\} $ sono un insieme massimale di vettori di $ V $ linearmente indipendenti allora $ \{v_1, \ldots, v_n\} $ è una base di $ V $. 
\end{thm}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $. Se $ v_1, \ldots, v_n $ un insieme di vettori di $ V $ linearmente indipendenti allora $ \{v_1, \ldots, v_n\} $ è una base di $ V $.
\end{thm}

\begin{thm}[completamento ad una base]
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $. Sia $ r $ un intero positivo con $ 0 < r < n $. Dati $ r $ vettori $ v_1, \ldots , v_r \in V $ linearmente indipendenti è possibile completarli ad una base di $ V $, ossia trovare vettori $ v_{r+1}, \ldots, v_n $ tali che $ \{v_1, \ldots , v_r, v_{r+1}, \ldots , v_n\} $ è base di $ V $.
\end{thm}